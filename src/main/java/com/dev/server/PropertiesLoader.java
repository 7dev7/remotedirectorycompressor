package com.dev.server;

import org.apache.log4j.Logger;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class PropertiesLoader {

    private static final Logger LOGGER = Logger.getLogger(PropertiesLoader.class);

    public static Properties loadProperties(String pathToProps) {
        try {
            FileInputStream fis = new FileInputStream(pathToProps);
            Properties props = new Properties();
            props.load(fis);
            return props;
        } catch (FileNotFoundException e) {
            LOGGER.error("Properties file not found [" + pathToProps + "] ", e);
            throw new RuntimeException("Properties file not found", e);
        } catch (IOException e) {
            LOGGER.error("Couldn't load properties [" + pathToProps + "] ", e);
            throw new RuntimeException("Couldn't load properties", e);
        }
    }

}
