package com.dev.server;

import org.apache.log4j.Logger;

import java.io.*;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Path;

public class ServerWorker {
    private static final Logger LOGGER = Logger.getLogger(ServerWorker.class);

    public void doWork(Socket socket) {
        try {
            ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream());
            ObjectInputStream in = new ObjectInputStream(socket.getInputStream());

            out.writeUTF("Input path to directory: ");
            out.flush();

            String pathToDirectory = in.readUTF();

            File file = new File(pathToDirectory);
            if (file.exists()) {
                Zipper zipper = new Zipper(file.getPath());
                zipper.zipDirectory();
            }

        } catch (IOException e) {
            LOGGER.error(e);
        } finally {
            try {
                socket.close();
            } catch (IOException e) {
                LOGGER.error("Couldn't close socket", e);
            }
        }
    }

    private void sendFile(ObjectOutputStream out, Path pathToFile) throws IOException {
        byte[] fileContent = Files.readAllBytes(pathToFile);
        out.writeUTF(pathToFile.getFileName().toString());
        out.writeObject(fileContent);
    }
}
