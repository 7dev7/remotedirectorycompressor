package com.dev.server;

public interface Constants {
    String DEFAULT_CONFIG_FILE = "src/main/resources/config.properties";
    String SERVER_PORT_PROP = "SERVER_PORT";
    String THREAD_POOL_PROP = "THREAD_POOL";
    String PATH_TO_PICS_PROP = "PATH_TO_PICS";
}
