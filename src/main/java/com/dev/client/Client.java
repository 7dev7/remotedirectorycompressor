package com.dev.client;

import org.apache.log4j.Logger;

import javax.swing.*;
import java.io.*;
import java.net.Inet4Address;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

public class Client {
    private static final int PORT = 14777;
    private static final Logger LOGGER = Logger.getLogger(Client.class);

    public void connect() {
        try (Socket socket = new Socket(Inet4Address.getLocalHost(), PORT)) {
            ObjectInputStream in = new ObjectInputStream(socket.getInputStream());
            ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream());

            String greeting = in.readUTF();
            System.out.println(greeting);

            String path = getUserInputWithValidation();

            out.writeUTF(path);
            out.flush();
        } catch (IOException e) {
            LOGGER.error(e);
            System.out.println("Couldn't connect to server:(");
        }
    }

    private String getUserInputWithValidation() {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            try {
                String input = reader.readLine();
                return input;
            } catch (IOException e) {
                LOGGER.error("Couldn't read value from server", e);
                throw new RuntimeException("Couldn't read line");
            }
        }
    }
}
